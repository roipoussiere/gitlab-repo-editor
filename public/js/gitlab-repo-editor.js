'use strict';

// constant values about GitLab api
const base_url = 'https://framagit.org';
const api_base_url = `${base_url}/api/v4`;
const client_id = '33583661464fe4b4771739144518ef699ffa3939c1ab1ed4e11843ce3c2dd86f';
const redirect_uri = 'https://roipoussiere.frama.io/gitlab-repo-editor';
const state = 'gitlab-repo-editor';
const scope = 'api';

const params = new URLSearchParams(window.location.hash.substr(1));
const access_token = params.get('access_token');
location.hash = '';

axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;
axios.defaults.headers.put['Content-Type'] = 'application/json';

function gbid(id) {
  return document.getElementById(id);
}

function on_load(icon_dom) {
  icon_dom.classList.add('loading');
}

function on_success(icon_dom, message = '') {
  icon_dom.classList.remove('loading');

  const success_msg_dom = gbid('success_msg');
  if (message) {
    success_msg_dom.innerHTML = message;
    success_msg_dom.style.display = 'block';
    setTimeout(() => {
      success_msg_dom.style.display = 'none';
      success_msg_dom.innerHTML = '';
    }, 4000);
  }
}

function on_error(icon_dom, message = '', debug_msg = 'An error occurred.') {
  icon_dom.classList.remove('loading');
  console.log(debug_msg);

  const error_msg_dom = gbid('error_msg');
  if (message) {
    error_msg_dom.innerHTML = message;
    error_msg_dom.style.display = 'block';
    setTimeout(() => {
      error_msg_dom.style.display = 'none';
      error_msg_dom.innerHTML = '';
    }, 4000);
  }
}

function get_user_info() {
  const icon_dom = gbid('author_name_icon');

  const api_endpoint = `${api_base_url}/user/`;

  on_load(icon_dom);
  axios.get(api_endpoint)
    .then(response => {
      if (response.data) {
        gbid('author_name').value = response.data['name'];
        gbid('author_email').value = response.data['email'];
        on_success(icon_dom);
      }
    })
    .catch(error => on_error(icon_dom, 'Can not get user information.', error));
}

function get_projects() {
  const project_id_dom = gbid('project_id');
  const icon_dom = gbid('project_id_icon');

  const api_endpoint = `${api_base_url}/projects/`;
  const url_params = { owned: true };

  on_load(icon_dom);
  axios.get(api_endpoint, {params: url_params})
    .then(response => {
      project_id_dom.innerHTML = '';
      if (response.data) {
        response.data.forEach(project => project_id_dom.innerHTML += `<option value="${project['id']}">${project['name']}</option>`);
        project_id_dom.selectedIndex = 0;
        on_success(icon_dom);
        get_git_branches();
      }
    })
    .catch(error => on_error(icon_dom, 'Can not get repositories.', error));
}

function get_git_branches() {
  const git_branch_dom = gbid('git_branch');
  const icon_dom = gbid('git_branch_icon');

  const project_id = encodeURIComponent(gbid('project_id').value);

  const api_endpoint = `${api_base_url}/projects/${project_id}/repository/branches`;

  on_load(icon_dom);
  axios.get(api_endpoint)
    .then(response => {
      git_branch_dom.innerHTML = '';
      if (response.data) {
        response.data.forEach(branch => git_branch_dom.innerHTML += `<option value="${branch['name']}">${branch['name']}</option>`);
        git_branch_dom.selectedIndex = 0;
        on_success(icon_dom);
        get_project_tree();
      }
    })
    .catch(error => on_error(icon_dom, 'Can not get repository branches.', error));
}

function get_project_tree() {
  const file_path_dom = gbid('file_path');
  const icon_dom = gbid('file_path_icon');

  const project_id = encodeURIComponent(gbid('project_id').value);

  const api_endpoint = `${api_base_url}/projects/${project_id}/repository/tree`;
  const url_params = {
    ref: encodeURIComponent(gbid('git_branch').value)
  };

  on_load(icon_dom);
  axios.get(api_endpoint, {params: url_params})
    .then(response => {
      file_path_dom.innerHTML = '';
      if (response.data) {
        response.data.filter(leaf => leaf.type === 'blob').forEach(leaf => file_path_dom.innerHTML += `<option value="${leaf['path']}">${leaf['path']}</option>`);
        file_path_dom.selectedIndex = 0;
        gbid('commit_message').value = `Update ${gbid('file_path').value}`;
        on_success(icon_dom);
        get_file_content();
      }
    })
    .catch(error => on_error(icon_dom, 'Can not get repository files list.', error));
}

function get_file_content() {
  const file_content_dom = gbid('file_content');
  const icon_dom = gbid('file_content_icon');

  const project_id = encodeURIComponent(gbid('project_id').value);
  const file_path = encodeURIComponent(gbid('file_path').value);

  const api_endpoint = `${api_base_url}/projects/${project_id}/repository/files/${file_path}/raw`;
  const url_params = {
    ref: encodeURIComponent(gbid('git_branch').value)
  };

  gbid('file_path_label').innerHTML = gbid('file_path').value;
  on_load(icon_dom);
  axios.get(api_endpoint, {params: url_params})
    .then(response => {
      file_content_dom.value = response.data;
      file_content_dom.disabled = false;
      on_success(icon_dom);
    })
    .catch(error => on_error(icon_dom, 'Can not get file content.', error));
}

function commit_content() {
  const icon_dom = gbid('commit_content_btn');

  const project_id = encodeURIComponent(gbid('project_id').value);
  const file_path = encodeURIComponent(gbid('file_path').value);

  const api_endpoint = `${api_base_url}/projects/${project_id}/repository/files/${file_path}`;
  const data = {
    branch: encodeURIComponent(gbid('git_branch').value),
    author_email: encodeURIComponent(gbid('author_email').value),
    author_name: encodeURIComponent(gbid('author_name').value),
    content: encodeURIComponent(gbid('file_content').value),
    commit_message: encodeURIComponent(gbid('commit_message').value)
  };

  on_load(icon_dom);
  axios.put(api_endpoint, data)
    .then(response => on_success(icon_dom, 'Content uploaded.'))
    .catch(error => on_error(icon_dom, 'Can not commit file content.', error));
}

function check_validation() {

}

if (access_token === null) {
  window.location.href = `${base_url}/oauth/authorize?client_id=${client_id}&redirect_uri=${encodeURIComponent(redirect_uri)}&response_type=token&state=${state}&scope=${scope}`;
} else {
  get_projects();
  get_user_info();
  gbid('project_id').onchange = () => get_git_branches();
  gbid('git_branch').onchange = () => get_project_tree();
  gbid('file_path').onchange = () => get_file_content();
  gbid('file_content').oninput = () => gbid('commit_group').disabled = false;
  gbid('commit_content_btn').onclick = () => commit_content();
  gbid('commit_message').oninput = () => gbid('commit_content_btn').disabled = ! gbid('commit_message').validity.valid;
  gbid('author_name').oninput = () => gbid('commit_content_btn').disabled = ! gbid('author_name').validity.valid;
  gbid('author_email').oninput = () => gbid('commit_content_btn').disabled = ! gbid('author_email').validity.valid;
}
