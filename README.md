# GitLab repository file editor

*A POC of a client-side app that edits a git repository file using the GitLab api.*

See it in action the [online demo](https://roipoussiere.frama.io/gitlab-repo-editor)!

![](./doc/app.jpg)

You will need to authorize the application to use you GitLab account, in order to read and write on your repositories:

![](./doc/authorization.jpg)

## Want to fork?

Please add a new application with `api` access on your own GitLab account, then edit the application id at the beginning of the [main js file](./public/js/gitlab-repo-editor.js).
